$(document).ready(function() {

    $('.btn-secondary').click( function(event){
        event.preventDefault();
        $('.modal')
            .css('display', 'block')
            .animate({opacity: 1, top: '30%'}, 200);
    });

    // закрытие модального окна
    $('.close').click( function(){
        $('.modal')
            .animate({opacity: 0, top: '45%'}, 200)
            .css('display', 'none');
    });

});
