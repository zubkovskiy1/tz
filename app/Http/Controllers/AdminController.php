<?php

namespace App\Http\Controllers;

use App\Report;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function admin()
    {
        return view('admin.admin');
    }

//

    public function userlist(Request $request)
    {
        $usersList = User::all();

        $userId = $request->input("user_id");



        if ($userId) {
            $reports = Report::where("user_id", $userId)->get();
            $user = User::where("id", $userId)->first();
            $total = Report::sumByUser($user->id);
//            var_dump($total);
            return view('admin.userlist', ["user" => $user, "reports" => $reports, "users" => $usersList, "total" => $total]);

        } else {
            return view('admin.userlist', ["users" => $usersList,"total" => "нет"]);
        }


    }
}
