<?php

namespace App\Http\Controllers;

use App\Report;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    public function index()
    {
        $users = Auth::user();
        return view('front.welcome', ['users' => $users]);
    }

    public function userboard()
    {
        $users = Auth::user();
        $reports = Report::where("user_id", "=", Auth::user()->id)->get();
        return view('front.userboard', ['users' => $users,'reports' => $reports, 'total' => Report::sumByUser($users->id)]);
    }
}
