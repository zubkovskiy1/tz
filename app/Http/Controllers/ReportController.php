<?php

namespace App\Http\Controllers;

use App\Report;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReportController extends Controller
{
    public function store(Request $request)
    {

        $this->validate($request, [
            'start_work' => 'required',
            'finish_work' => 'required',
        ]);


        $start = new \DateTime($request->post("start_work"));
        $end = new \DateTime($request->post("finish_work"));


        $report = Report::add([$start, $end]);

        return redirect()->route('userboard');
    }

    public function getHours(Request $request)
    {
        $entity = Report::where("id", $request->post("report_id"))->first();
        $entity->getMoneyAndHours();
    }

    public function getSumByUser(Request $request)
    {
        var_dump(Report::sumByUser());

    }
}
