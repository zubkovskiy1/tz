<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class LoginController extends Controller
{
    public function index()
    {
        $users = Auth::user();
        return view('front.login',['users' => $users]);
    }

    public function login(Request $request)
    {
        $this->validate($request,[
            'email' => 'required|email',
            'password' => 'required'
        ]);
       if(
           Auth::attempt([
            'email' => $request->get('email'),
            'password' => $request->get('password')
           ])
       )
        {
            return redirect('/userboard');
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
