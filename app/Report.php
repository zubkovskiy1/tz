<?php

namespace App;

use DateTime;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Report extends Model
{

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    protected $fillable = ['start_work','finish_work'];


    public static function add($fields)
    {
        $report = new static;
        $report->start_work = $fields[0];
        $report->finish_work = $fields[1];
        $report->user_id = Auth::user()->id;
        $report->save();

        return $report;
    }

    public function getMoneyAndHours()
    {

        $start = new DateTime($this->start_work);
        $end = new DateTime($this->finish_work);

        $hours = date_diff($end, $start);

        return [
            "hours" => $hours->h,
            "money" => $hours->h * 30
        ];

    }


    public static function sumByUser($userId) {
        $reports = Report::where("user_id", "=", $userId)->get();

        $sum = 0;

        foreach ($reports as $report) {
            $moneyHours = $report->getMoneyAndHours();
            $sum = $sum + $moneyHours["money"];
        }

        return $sum;

    }
}
