
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    @if(Auth::check())
        <a class="navbar-brand" href="#">TZ</a>
    @else
        <a class="navbar-brand" href="{{ route('welcome') }}">TZ</a>
    @endif
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse " id="navbarColor01">
@if(Auth::check())
        <div class="top-right links">
            <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                {{--<button type="button" class="btn btn-primary">{{$users->name}}</button>--}}
                <a class="btn btn-primary" href="{{ route('userboard') }}">{{$users->name}}</a>
                <div class="btn-group show" role="group">
                    <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"></button>
                    <div class="dropdown-menu show" aria-labelledby="btnGroupDrop1" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 48px, 0px); top: 0px; left: -111px; will-change: transform;">
                        <a class="dropdown-item" href="{{ route('logout') }}">Logout</a>
                    </div>
                </div>
            </div>
        </div>
@else
        <ul class="navbar-nav  my-2 my-lg-0">
            <li class="nav-item {{(url()->current()==url('/register/') ? ' active ' : null)}}">
                <a class="nav-link" href="{{ route('register') }}">Registration</a>
            </li>
            <li class="nav-item {{(url()->current()==url('/login/') ? ' active ' : null)}}">
                <a class="nav-link" href="{{ route('login') }}">Login</a>
            </li>
        </ul>
@endif

    </div>
</nav>