@extends('layout')
@include('front._header')
@section('content')
<div class="container">
    <h1>Register yourself</h1>
    <form class="form-horizontal contact-form" role="form" method="post" action="/register">
        {{csrf_field()}}

        <div class="form-group">
            <label class="col-form-label" for="inputDefault">Name</label>
            <input type="text" class="form-control" name="name" placeholder="Name" id="name" required>
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input type="text" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" required>
            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>

        <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password" required>
        </div>

        <button type="submit" class="btn send-btn">Register</button>

    </form>
</div>

@endsection