@extends('layout')
@include('front._header')
@section('content')
    <div class="container">
        <button type="button" class="btn btn-secondary">Add information</button>
        <table class="table table-hover">
            <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Name</th>
                <th scope="col">Hours</th>
                <th scope="col">Price</th>
            </tr>
            </thead>
            <tbody>
            @foreach($reports as $report)
                <tr class="table-dark">

                    <td scope="row">{{$users->id}}</td>
                    <td>{{$users->name}}</td>
                    <td>{{$report->getMoneyAndHours()["hours"]}}</td>
                    <td>{{$report->getMoneyAndHours()["money"]}}</td>

                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="card text-white bg-primary mb-3" style="max-width: 20rem;">
            <div class="card-header">Total</div>
            <div class="card-body">
                <h4 class="card-title">{{$total}} $</h4>
            </div>
        </div>
        <div class="modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal contact-form" role="form" method="post" action="/report">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="">Время начала работы</label>
                                <input type="datetime-local" name="start_work" class="form-control" id=""
                                       aria-describedby="" placeholder="время начало работы" required>
                            </div>

                            <div class="form-group">
                                <label for="">Время окончание работы</label>
                                <input type="datetime-local" name="finish_work" class="form-control" id=""
                                       placeholder="Время окончания работы" required>
                            </div>

                            <button type="submit" class="btn send-btn">Отправить</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection