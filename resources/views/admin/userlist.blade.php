@extends('admin.admin')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper  " style='min-height: 100%;'>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">
                <!-- /.box-header -->

                <div class="box-body">
                    <a class="btn btn-primary" href="{{route('userboard')}}">Вернуться назад</a>
                        <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                            <button type="button" class="btn btn-info">Select User</button>
                            <div class="btn-group" role="group">
                                <button id="btnGroupDrop3" type="button" class="btn btn-info dropdown-toggle"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop3">
                                    @foreach($users as $User)
                                        <a class="dropdown-item"
                                           href="{{url("/admin/users?user_id=" . $User->id)}}">{{$User->name}}</a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Имя</th>
                                <th>E-mail</th>
                                <th>Количество часов</th>
                                <th>Сумма заработка за день</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if(isset($reports))
                            @foreach($reports as $report)
                                <tr>
                                    <td>{{$user->id}}</td>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>{{$report->getMoneyAndHours()["hours"]}}</td>
                                    <td>{{$report->getMoneyAndHours()["money"]}}</td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                    <div class="card bg-light mb-3" style="max-width: 20rem;">
                        <div class="card-header">Total</div>
                        <div class="card-body">
                            <h4 class="card-title">{{$total}} $</h4>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection