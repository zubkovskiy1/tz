<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware'	=>	'guest'], function(){
    Route::get('/login','LoginController@index')->name('login');
    Route::post('/login', 'LoginController@login');
    Route::get('/register','RegisterController@index')->name('register');
    Route::post('/register', 'RegisterController@store');
    Route::get('/','IndexController@index')->name('welcome');
});

Route::group(['middleware'	=>	'auth'], function(){
    Route::get('/userboard','IndexController@userboard')->name('userboard');
    Route::get('/logout', 'LoginController@logout')->name('logout');
    Route::get('/userboard','IndexController@userboard')->name('userboard');
    Route::post('/report', 'ReportController@store');
    Route::get('/getHours', 'ReportController@getHours');
    Route::get('/getSumByUser', 'ReportController@getSumByUser');
});

Route::group(['prefix'=>'admin', 'middleware'	=>	'admin'], function(){
    Route::get('/', 'AdminController@admin')->name('admin');
    Route::get('/users', 'AdminController@userlist')->name('userlist');
});







